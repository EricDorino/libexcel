/*
 * Copyright (C) 2018, 2021 Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include "biff.h"
#include "xls.h"

struct xls {
    XLS_format fmt;
    FILE *file;
    size_t maxrow,
           maxcol;
    ssize_t nfonts,     /* -1 .. inf */
            nformats,   /* _n_builtin .. inf */   
            nxf;        /* Extended format: -1 .. 62 */
    struct {            /* Extended format */
        font_type font;
        number_format_type format;
    } xf_def[63];
};

struct xls *xls_open(const char *name, XLS_format fmt) {
    if (!name)
        return NULL;
    struct xls *xls = malloc(sizeof(struct xls));
    unsigned char buf[1024];
    size_t len;

    if (xls) {
        xls->fmt = fmt;
        xls->maxrow = xls->maxcol = 0;
        xls->nfonts = xls->nxf = -1;
        xls->nformats = biff[xls->fmt].n_builtin_formats(); 
        if (!(xls->file = fopen(name, "w"))) {
            free(xls);
            xls = NULL;
        }
        len = biff[xls->fmt].bof(buf, sizeof buf); 
        fwrite(buf, 1, len, xls->file);
    }
    return xls;
}

void xls_close(struct xls *xls) {
    unsigned char buf[64];
    size_t len;

    if (xls) {
        len = biff[xls->fmt].eof(buf, sizeof buf, xls->maxrow, xls->maxcol); 
        fwrite(buf, 1, len, xls->file);
        fclose(xls->file);
        free(xls);
    }
}

void xls_page_setup(struct xls *xls) { 
    unsigned char buf[32];
    size_t len;

    if (xls) {
        len = biff[xls->fmt].page_setup(buf, sizeof buf); 
        fwrite(buf, 1, len, xls->file);
    }
}

void xls_header(struct xls *xls, const char *s) {
    unsigned char *buf;
    size_t len;

    if (!xls || !s)
        return;
    if (!(buf = malloc(len = 5 + strlen(s))))
        return;
    len = biff[xls->fmt].header(buf, len, s);
    fwrite(buf, 1, len, xls->file);
    free(buf);
}

void xls_footer(struct xls *xls, const char *s) {
    unsigned char *buf;
    size_t len;

    if (!xls || !s)
        return;
    if (!(buf = malloc(len = 5 + strlen(s))))
        return;
    len = biff[xls->fmt].footer(buf, len, s);
    fwrite(buf, 1, len, xls->file);
    free(buf);
}

extern void xls_row_col_headers(struct xls *xls) {
    unsigned char buf[6];
    size_t len;

    if (!xls)
        return;
    len = biff[xls->fmt].row_col_headers(buf, sizeof buf); 
    fwrite(buf, 1, len, xls->file);
}

extern void xls_grid_lines(struct xls *xls) {
    unsigned char buf[6];
    size_t len;

    if (!xls)
        return;
    len = biff[xls->fmt].grid_lines(buf, sizeof buf); 
    fwrite(buf, 1, len, xls->file);
}

extern font_type xls_define_font(struct xls *xls,
                                const char *name, size_t size,
                                font_style style, color color) {
    unsigned char *buf;
    size_t len, buflen;

    if (!name || !xls)
        return -1;
    if (!(buf = malloc(buflen = 32 + strlen(name))))
        return -1;
    xls->nfonts++;
    if (xls->nfonts == 4)
        xls->nfonts += 1;       /* Bug in all BIFF versions */
    len = biff[xls->fmt].define_font(buf, buflen, 
                    name, size, style, color); 
    fwrite(buf, 1, len, xls->file);
    free(buf);
    return xls->nfonts;
}

number_format_type xls_define_number_format(struct xls *xls, const char *fmt) {
    unsigned char *buf;
    size_t len, buflen;

    if (!(xls && fmt))
        return -1;
    if (!(buf = malloc(buflen = 4 + strlen(fmt))))
        return -1;
    len = biff[xls->fmt].define_number_format(buf, buflen, fmt);
    fwrite(buf, 1, len, xls->file);
    free(buf);
    return ++xls->nformats;
}

cell_format_type xls_define_cell_format(struct xls *xls,
                              font_type font, 
                              number_format_type format, 
                              align align, border border,
                              int shaded) {
    unsigned char buf[12];
    size_t len;

    if (!xls)
        return -1;
    len = biff[xls->fmt].define_cell_format(buf, sizeof buf,
                                    font, format, align, border, shaded);
    if (len <= 0)
        return -1;
    fwrite(buf, 1, len, xls->file);
    xls->nxf++;
    if (xls->nxf < sizeof(xls->xf_def) / sizeof(xls->xf_def[0])) {
        xls->xf_def[xls->nxf].format = format;
        xls->xf_def[xls->nxf].font = font;
        return xls->nxf;
    }
    return -1;
}

static void store_row_col(struct xls *xls, size_t row, size_t col)
{
    if (row > xls->maxrow)
        xls->maxrow = row;
    if (col > xls->maxcol)
        xls->maxcol = col;
}

void xls_write_int(struct xls *xls,
        unsigned row, unsigned col,
        cell_format_type format,
        int data) {
    unsigned char buf[64];
    size_t len;

    if (xls && format >= 0 && format <= xls->nxf) {
        store_row_col(xls, row, col);
        if (data >= 0 && data <= 65635)
            len = biff[xls->fmt].data_uint16(buf, sizeof buf,
                    row, col, 
                    xls->xf_def[format].format, 
                    format,
                    xls->xf_def[format].font,
                    (uint16_t)data);
        else
            len = biff[xls->fmt].data_double(buf, sizeof buf,
                    row, col, 
                    xls->xf_def[format].format, 
                    format,
                    xls->xf_def[format].font,
                    (double)data);
        fwrite(buf, 1, len, xls->file);
    }
}

void xls_write_double(struct xls *xls,
        unsigned row, unsigned col,
        cell_format_type format,
        double data) {
    unsigned char buf[64];
    size_t len;

    if (xls && format >= 0 && format <= xls->nxf) {
        store_row_col(xls, row, col);
        len = biff[xls->fmt].data_double(buf, sizeof buf,
                row, col, 
                xls->xf_def[format].format, 
                format,
                xls->xf_def[format].font,
                data);
        fwrite(buf, 1, len, xls->file);
    }
}

void xls_write_str(struct xls *xls,
        unsigned row, unsigned col,
        cell_format_type format,
        const char *data) {
    unsigned char *buf;
    size_t len, buflen;

    buf = malloc(buflen = 16+strlen(data));

    if (xls && format >= 0 && format <= xls->nxf && buf) {
        store_row_col(xls, row, col);
        len = biff[xls->fmt].data_str(buf, buflen,
                row, col, 
                xls->xf_def[format].format, 
                format,
                xls->xf_def[format].font,
                data);
        fwrite(buf, 1, len, xls->file);
        free(buf);
    }
}

