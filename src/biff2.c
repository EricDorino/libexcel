/*
 * Copyright (C) 2018, 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include "biff2.h"
#include "xls.h"

#define XLS_WORKBOOK    0x05
#define XLS_VBMODULE    0x06
#define XLS_SHEET       0x10
#define XLS_CHART       0x20
#define XLS_MACROSHEET  0x40

static struct {
    number_format_type fid;
    const char *str;
} builtin_formats[] = {
    { NUMBER_FORMAT_GENERAL,         "General"},
    { NUMBER_FORMAT_DECIMAL0,        "0"},
    { NUMBER_FORMAT_DECIMAL2,        "0.00"},
    { NUMBER_FORMAT_DECIMAL0_THOUSANDS_SEPARATOR, "#'##0"},
    { NUMBER_FORMAT_DECIMAL2_THOUSANDS_SEPARATOR, "#'##0.00"},
    { NUMBER_FORMAT_CURRENCY0,       "$ #'##0;$ -#'##0"},
    { NUMBER_FORMAT_CURRENCY_RED0,   "$ #'##0;$ -#'##0"},
    { NUMBER_FORMAT_CURRENCY2,       "$ #'##0.00;$ -#'##0.00"},
    { NUMBER_FORMAT_CURRENCY_RED2,   "$ #'##0.00;$ -#'##0.00"},
    { NUMBER_FORMAT_PERCENT0,        "0%"},
    { NUMBER_FORMAT_PERCENT2,        "0.00%"},
    { NUMBER_FORMAT_SCIENTIFIC,      "0.00E+00"},
    { NUMBER_FORMAT_PERCENT0_PLUS,   "+0%;-0%;0%"},
    { NUMBER_FORMAT_PERCENT2_PLUS,   "+0.00%;-0.00%;0.00%"}
};


size_t biff2_bof(uint8_t *buf, size_t size) {
    size_t len;
    if (size < 34)
        return 0;
    /*
     * 5.8 BOF beginning of file
     */
    len = biff_list(buf, size, 0x09, "dd", 2, XLS_SHEET);
    /* 5.17 CODEPAGE */
    len += biff_uint16(buf+len, size-len, 0x42, 0x8001); /* Windows CP-1252 */
    /* 5.14 CALCMODE */
    len += biff_uint16(buf+len, size-len, 0x0d, 1); /* Automatic */
    /* 5.85 REFMODE */
    len += biff_uint16(buf+len, size-len, 0x0f, 1); /* A1 mode */
    /* 5.28 DATEMODE */
    len += biff_uint16(buf+len, size-len, 0x22, 1); /* 1904 date system */
    /*
     * Builtin formats
     */
    len += biff_uint16(buf+len, size-len, 0x1f, biff2_n_builtin_formats()-1);
    for (size_t i = 0; i < biff2_n_builtin_formats(); i++) {
        if (size < 2 + strlen(builtin_formats[i].str))
            return 0;
        len += biff_str(buf+len, size-len, 0x1e, builtin_formats[i].str);
    }
    /*
     * 5.35 DIMENSION
     */
    return len + biff_list(buf+len, size-len, 0x0, "dddd", 0, 1, 0, 1);
}

size_t biff2_eof(uint8_t *buf, size_t size, size_t maxrow, size_t maxcol) {
    size_t len;
    if (size < 14)
        return 0;
    /*
     * 5.35 DIMENSION
     */
    len = biff_list(buf, size, 0x0, "dddd", 0, maxrow+1, 0, maxcol+1);
    /*
     * 5.9 EOF end of file
     */
    return len + biff_uint16(buf+len, size-len, 0x0a, 0);
}

size_t biff2_page_setup(uint8_t *buf, size_t size) {
    size_t len;
    if (size < 18)
        return 0;
    
    /*
     * Warning: this is BIFF3+.
     * This field contains other informations.
     * 5.97 p207
     */
    len = biff_uint16(buf, size, 0x81, 0);  /* scale == true */
    /*
     * Warning: this is BIFF4
     */
    return len + biff_list(buf+len, size-len, 
                           0xa1,
                           "dddddd",
                           0,           /* paper type undefined */
                           100,         /* scaling_percent */
                           1,           /* start page */
                           1,           /* fit_width_with_n_pages */
                           1,           /* fit_height_with_n_pages */
                           2);          /* orientation == PORTRAIT */
}

size_t biff2_header(uint8_t *buf, size_t size, const char *s) {
    /*
     * 5.55 p180
     */
    return biff_str(buf, size, 0x14, s);
}

size_t biff2_footer(uint8_t *buf, size_t size, const char *s) {
    /*
     * 5.48 p173
     */
    return biff_str(buf, size, 0x15, s);
}

size_t biff2_row_col_headers(uint8_t *buf, size_t size) {
    /*
     * 5.81 p199
     */
    return biff_uint16(buf, size, 0x2a, 1);
}

size_t biff2_grid_lines(uint8_t *buf, size_t size) {
    /*
     * 5.80 p199
     */
    return biff_uint16(buf, size, 0x2b, 1);
}

size_t biff2_define_font(uint8_t *buf, size_t size,
                         const char *font_name, size_t font_size,
                         font_style font_style, color font_color) {
    size_t len;
    
    /*
     * 5.45 FONT p.171
     */
    len = biff_list(buf, size, 0x31, "dds",
            font_size*20,
            font_style,
            font_name);
    if (len > 0 && font_color != COLOR_AUTO) 
        /*
         * 5.47 FONTCOLOR
         */
        len += biff_list(buf+len, size-len, 0x45, "d", font_color);
    return len;
}   

size_t biff2_define_number_format(uint8_t *buf, size_t size, const char *fmt) {
    return biff_str(buf, size, 0x1e, fmt);
}

size_t biff2_define_cell_format(uint8_t *buf, size_t size,
                           font_type font, number_format_type fmtn, 
                           align align, border border, int shaded) {
    /*
     * 5.115 XF - Extended Format
     * 5.115.2 - BIFF2 XF Record Contents
     */
    return biff_list(buf, size, 0x43, "cccc",
                     font, 
                     0,     /* Not used */
                     fmtn/*+locked?1<<6:0+formula_hiddent?1<<7:0*/, 
                     align+(border<<3)+(shaded?1<<7:0));
}

size_t biff2_n_builtin_formats(void) {
    return sizeof(builtin_formats)/sizeof(builtin_formats[0]);
}

static size_t cell_attr(uint8_t *buf, size_t size,
                        number_format_type number_format, 
                        cell_format_type cell_format,
                        font_type font) {
    if (size < 3)
        return 0;
    buf[0] = cell_format;
    buf[1] = (number_format | 0xc0) & font;
    buf[2] = 0;
    return 3;
}

size_t biff2_data_uint16(uint8_t *buf, size_t size, 
                         unsigned row, unsigned col,
                         number_format_type number_format,
                         cell_format_type cell_format,
                         font_type font,
                         uint16_t val) {
    /*
     * 5.60 INTEGER
     */
    size_t len = biff_list(buf, size, 0x02, "ddad", row, col, "XXX", val);
    if (len > 0)
        (void)cell_attr(buf+8, 3, number_format, cell_format, font);
    return len;
}

size_t biff2_data_double(uint8_t *buf, size_t size, 
                         unsigned row, unsigned col,
                         number_format_type number_format,
                         cell_format_type cell_format,
                         font_type font,
                         double val) {
    /*
     * 5.71 NUMBER
     */
    size_t len = biff_list(buf, size, 0x03, "ddaf", row, col, "XXX", val);
    if (len > 0)
        (void)cell_attr(buf+8, 3, number_format, cell_format, font);
    return len;
}
size_t biff2_data_str(uint8_t *buf, size_t size, 
                      unsigned row, unsigned col ,
                      number_format_type number_format,
                      cell_format_type cell_format,
                      font_type font,
                      const char *val) {
    /*
     * 5.63 LABEL
     */
    size_t len = biff_list(buf, size, 0x04, "ddas", row, col, "XXX", val);
    if (len > 0)
        (void)cell_attr(buf+8, 3, number_format, cell_format, font);
    return len;
}
