/*
 * Copyright (C) 2018, 2021 Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIFF_H
#define BIFF_H

#include <stdio.h>
#include "xls.h"

extern struct biff {
    /* BOF and EOF */
    size_t (*bof)(uint8_t *, size_t);
    size_t (*eof)(uint8_t *, size_t, size_t, size_t);
    /* Page setup */
    size_t (*page_setup)(uint8_t *, size_t);
    size_t (*header)(uint8_t *, size_t, const char *);
    size_t (*footer)(uint8_t *, size_t, const char *);
    /* Render setup */
    size_t (*row_col_headers)(uint8_t *, size_t);
    size_t (*grid_lines)(uint8_t *, size_t);
    /* Fonts */
    size_t (*define_font)(uint8_t *, size_t,
                                    const char *, size_t, font_style, color);
    /* Formats */
    size_t (*define_number_format)(uint8_t *, size_t, const char *);
    size_t (*define_cell_format)(uint8_t *, size_t,
                            font_type, number_format_type,
                            align, border, int);
    size_t (*n_builtin_formats)(void);
    /* Data */
    size_t (*data_uint16)(uint8_t *, size_t, unsigned, unsigned,
                          number_format_type,
                          cell_format_type,
                          font_type,
                          uint16_t);
    size_t (*data_double)(uint8_t *, size_t, unsigned, unsigned,
                          number_format_type,
                          cell_format_type,
                          font_type,
                          double);
    size_t (*data_str)(uint8_t *, size_t, unsigned, unsigned,
                       number_format_type,
                       cell_format_type,
                       font_type,
                       const char *);
} biff[];

extern size_t biff_list(uint8_t *, size_t, unsigned, const char *, ...);

extern size_t biff_uint16(uint8_t *, size_t, unsigned, uint16_t);
extern size_t biff_double(uint8_t *, size_t, unsigned, double);
extern size_t biff_str(uint8_t *, size_t, unsigned, const char *);

#endif
