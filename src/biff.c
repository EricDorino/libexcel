/*
 * Copyright (C) 2018, 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <string.h>
#include "ieee754.h"
#include "biff.h"
#include "biff2.h"
#include "xls.h"

struct biff biff[] = {
    /* BIFF2 */ {
        biff2_bof,
        biff2_eof,
        biff2_page_setup,
        biff2_header,
        biff2_footer,
        biff2_row_col_headers,
        biff2_grid_lines,
        biff2_define_font,
        biff2_define_number_format,
        biff2_define_cell_format,
        biff2_n_builtin_formats,
        biff2_data_uint16,
        biff2_data_double,
        biff2_data_str,
    },
};

static size_t intel_uint16(uint8_t *buf, size_t size, uint16_t val) {
    if (size >= 2) {
        buf[0] = val & 255;
        buf[1] = (val / 256) & 255;
    }
    return 2;   
}

static size_t intel_double(uint8_t *buf, size_t size, double val) {
    if (size >= 8)
        (void)double_to_ieee754(val, buf);
    return 8;
}

size_t biff_list(uint8_t *buf, size_t size, 
                 unsigned id, const char *fmt, ...) {
    va_list ap;
    size_t len = 4;

    if (size < 4)
        return 0;
    va_start(ap, fmt);
    while (*fmt && size > len) {
        switch (*fmt++) {
            case 'd':   /* 2 bytes integer big endian */
                len += intel_uint16(buf+len, size-len,
                                    (uint16_t)va_arg(ap, int));
                break;  
            case 'f':   /* ieee754 64 bits floating point */
                len += intel_double(buf+len, size-len, va_arg(ap, double));
                break;
            case 'c':   /* 1 byte integer */
                *(buf+len) = (uint8_t)va_arg(ap, int);
                len += 1;
                break;
            case 's':   /* string:
                           1-byte length,
                           n-bytes characters, no terminating zero
                         */
                {
                    char *s = va_arg(ap, char *);
                    size_t slen = s ? strlen(s) : 0;
                    if (slen > 0 && slen <= 255 && size >= len + slen + 1) {
                        *(buf+len) = (uint8_t)slen;
                        memcpy(buf+len+1, s, slen);
                    }
                    len += slen + 1;
                }
                break;
            case 'a':   /* string:
                           n-bytes characters, no terminating zero
                         */
                {
                    char *s = va_arg(ap, char *);
                    size_t slen = s ? strlen(s) : 0;
                    if (slen > 0 && slen <= 255 && size >= len + slen) {
                        memcpy(buf+len, s, slen);
                    }
                    len += slen;
                }
                break;
        }
        if (size < len)
            return 0;
    }
    va_end(ap);
    (void)intel_uint16(buf, 2, id);
    (void)intel_uint16(buf+2, 2, len-4);
    return len;
}

size_t biff_uint16(uint8_t *buf, size_t size, unsigned id, uint16_t val) {
    return biff_list(buf, size, id, "d", val);
}

size_t biff_double(uint8_t *buf, size_t size, unsigned id, double val) {
    return biff_list(buf, size, id, "f", val);
}

size_t biff_str(uint8_t *buf, size_t size, unsigned id, const char *val) {
    return biff_list(buf, size, id, "s", val);
}
