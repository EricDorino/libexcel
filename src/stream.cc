/*
 * Copyright (C) 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cstring>
#include <iostream>
#include <stdexcept>

#include "biff.h"
#include "oxlstream"

ed::oxlstream::oxlstream(ed::excel_format format):
    std::ofstream(),
    _excel_format(format),
    _nfonts(-1),
    _nformats(_n_builtin_formats()),
    _nxf(-1),
    _opened(false),
    _max_row(0),
    _max_col(0),
    _row(1),
    _col(1),
    _xf_in_use(0)
{
    // Nothing
}

ed::oxlstream::oxlstream(const char * filename, ed::excel_format format):
    std::ofstream(filename, std::ios_base::out),
    _excel_format(format),
    _nfonts(-1),
    _nformats(_n_builtin_formats()),
    _nxf(-1),
    _opened(true),
    _max_row(0),
    _max_col(0),
    _row(1),
    _col(1),
    _xf_in_use(0)
{
    _bof();
}

ed::oxlstream::~oxlstream()
{
    if (_opened)
        _eof();
}

void ed::oxlstream::open(const char * filename)
{
    if (_opened)
        throw std::logic_error("oxlstream already opened");
    std::ofstream::open(filename, std::ios_base::out);
    _opened = true;
    _bof();
}

void ed::oxlstream::close()
{
    if (!_opened)
        throw std::logic_error("oxlstream already closed");
    _eof();
    _opened = false;
}

// Page setup

#if 0
#define WORKBOOK    0x05
#define VBMODULE    0x06
#define SHEET       0x10
#define CHART       0x20
#define MACROSHEET  0x40
#endif

void ed::oxlstream::_bof()
{
    // 5.8  BOF beginning of file
#if 0
    std::vector<uint8_t> v;

    switch (_excel_format) {
        case BIFF2:
        default:
            v.reserve(4);
            cat(v, _intel16(2));
            cat(v, _intel16(SHEET));
            _biff(0x09, v);
            break;
    }
    
    // 5.17 CODEPAGE
    _biff(0x0042, _intel16(0x8001));    // Windows CP-1252
    // 5.14 CALCMODE
    _biff(0x000d, _intel16(1));         // AUtomatic
    // 5.85 REFMODE
    _biff(0x000f, _intel16(1));         // A1 mode
    // 5.28 DATEMODE
    _biff(0x0022, _intel16(1));         // 1904 date system

    _builtin_formats();

    _dimensions();

    _default_font = define_font("DejaVu Sans", 10);

    // Default format
    _default_cell_format = define_cell_format(_default_font, General);

    // BIFF3+ format styles
    _cma_format = define_cell_format(_default_font, Decimal_2);
    _ccy_format = define_cell_format(_default_font, Currency_0);
    _pct_format = define_cell_format(_default_font, Percent_0);

    // BIFF3+ styles 5.103 STYLE p.212
#define COMMA_STYLE     3
#define CURRENCY_STYLE  4
#define PERCENT_STYLE   5
    _define_style(_cma_format, COMMA_STYLE);
    _define_style(_ccy_format, CURRENCY_STYLE);
    _define_style(_pct_format, PERCENT_STYLE);
#else
    unsigned char buf[1024];
    size_t len;

    len = biff[_excel_format].bof(buf, sizeof buf);
    std::ofstream::write((const char *)buf, len);
    
    _default_font = define_font("DejaVu Sans", 10); // FIXME as parameters
    _default_cell_format = define_cell_format(_default_font, General);
#endif
}

void ed::oxlstream::_eof()
{
    // 5.9  EOF end of file
#if 0
    _dimensions();
    _biff(0x0a, _intel16(0));
#else
    unsigned char buf[64];
    size_t len;

    len = biff[_excel_format].eof(buf, sizeof buf, _max_row, _max_col);
    std::ofstream::write((const char *)buf, len);
#endif
}

#if 0
// 5.35 DIMENSION
void ed::oxlstream::_dimensions()
{
    std::vector<uint8_t> v;

    switch (_excel_format) {
        case BIFF2:
        default:
            v.reserve(16);
            cat(v, _intel16(0));
            cat(v, _intel16(_max_row+1));
            cat(v, _intel16(_max_col+1));
            _biff(0x0, v);
            break;
    }
}
#endif

void ed::oxlstream::page_setup()
{
#if 0
    // Warning: this is BIFF3+.
    // This field contains other informations.
    // 5.97 p207
    _biff(0x81, _intel16(scale ? 0 : 0x7fff));

    // Warning: this is BIFF4.
    std::vector<uint8_t> v;

    v.reserve(12);
    cat(v, _intel16(0));        // paper type undefined
    cat(v, _intel16(scaling_percent));
    cat(v, _intel16(1));        // start page #
    cat(v, _intel16(fit_width_with_n_pages));
    cat(v, _intel16(fit_height_with_n_pages));
    cat(v, _intel16(orientation == Landscape ? 0 : 2));
    _biff(0xa1, v);
#else
    unsigned char buf[32];
    size_t len;

    len = biff[_excel_format].page_setup(buf, sizeof buf); 
    std::ofstream::write((const char *)buf, len);
#endif
}

void ed::oxlstream::header(const char *s)
{
#if 0
    // 5.55 p180
    if (s)
        _biff(0x14, s);
#else
    if (s) {
        size_t len;
        unsigned char *buf = new unsigned char[len = 5 + strlen(s)];
    
        len = biff[_excel_format].header(buf, len, s);
        std::ofstream::write((const char *)buf, len);
        delete buf;
    }
#endif
}

void ed::oxlstream::footer(const char *s)
{
#if 0
    // 5.48 p173
    if (s)
        _biff(0x15, s);
#else
    if (s) {
        size_t len;
        unsigned char *buf = new unsigned char[len = 5 + strlen(s)];
    
        len = biff[_excel_format].footer(buf, len, s);
        std::ofstream::write((const char *)buf, len);
        delete buf;
    }
#endif
}

//
// Render setup

void ed::oxlstream::row_col_headers()
{
#if 0
    // 5.81 p199
    _biff(0x2a, _intel16(1));
#else
    unsigned char buf[16];
    size_t len;

    len = biff[_excel_format].row_col_headers(buf, sizeof buf);
    std::ofstream::write((const char *)buf, len);
#endif
}

void ed::oxlstream::grid_lines()
{
#if 0
    // 5.80 p199
    _biff(0x2b, _intel16(1));
#else
    unsigned char buf[16];
    size_t len;

    len = biff[_excel_format].grid_lines(buf, sizeof buf);
    std::ofstream::write((const char *)buf, len);
#endif
}

// 5.45 FONT

ed::font_type ed::oxlstream::define_font(
                const std::string &name, size_t size,
                font_style style)
{
    _nfonts++;
    if (_nfonts == 4)
        _nfonts++;      // Anomaly/bug in all BIFF versions
#if 0
    std::vector<uint8_t> v;

    switch (_excel_format) {
        case BIFF2:
        default:
            // 5.45 FONT p.171
            cat(v, _intel16(size*20)); 
            cat(v, _intel16(style));
            cat(v, _to_buf(name));
            _biff(0x31, v);
            if (color != Auto) 
                // 5.47 FONTCOLOR
                _biff(0x45, _intel16(color));
            break;
    }
#else
    size_t len;
    unsigned char *buf = new unsigned char[len = 32 + name.length()];
    
    len = biff[_excel_format].define_font(buf, len, 
                name.c_str(), size,
                (::font_style)style,
                COLOR_AUTO);
    std::ofstream::write((const char *)buf, len);
    delete buf;
#endif
#if 0
    std::cerr << "define_font(): " << name << "/" << size << " ";
    std::cerr << "index=" << _nfonts << std::endl;
#endif
    return _nfonts;
}

// Formats

static struct {
    ed::number_format_type fid;
    const char *str;
} _b_fmts[] = {
    { ed::General,          "General"},
    { ed::Decimal_0,        "0"},
    { ed::Decimal_2,        "0.00"},
    { ed::Decimal_0_Thousands_Separator, "#'##0"},
    { ed::Decimal_2_Thousands_Separator, "#'##0.00"},
    { ed::Currency_0,       "$ #'##0;$ -#'##0"},
    { ed::Currency_Red_0,   "$ #'##0;$ -#'##0"},
    { ed::Currency_2,       "$ #'##0.00;$ -#'##0.00"},
    { ed::Currency_Red_2,   "$ #'##0.00;$ -#'##0.00"},
    { ed::Percent_0,        "0%"},
    { ed::Percent_2,        "0.00%"},
    { ed::Scientific,       "0.00E+00"},
    { ed::Percent_0_Plus,   "+0%;-0%;0%"},
    { ed::Percent_2_Plus,   "+0.00%;-0.00%;0.00%"}
};

void ed::oxlstream::_builtin_formats()
{
    _biff(0x1f, _intel16(sizeof(_b_fmts)/sizeof(_b_fmts[0])-1));
    for (size_t i = 0; i < sizeof(_b_fmts)/sizeof(_b_fmts[0]); i++)
        _biff(0x1e, _to_buf(_b_fmts[i].str));
}

ssize_t ed::oxlstream::_n_builtin_formats() const
{
    return sizeof(_b_fmts)/sizeof(_b_fmts[0])-1;
}

// 5.115 XF - Extended format
 
ed::cell_format_type ed::oxlstream::define_cell_format(
        font_type font,
        number_format_type format,
        horizontal_align align,
        cell_border border,
        bool shaded)
{
#if 0
    std::vector<uint8_t> v;

    switch (_excel_format) {
        case BIFF2:
        default:
            v.resize(4);
            v[0] = uint8_t(font);
            v[1] = 0;
            v[2] = uint8_t(format);
            v[3] = uint8_t(align + (border << 3) + (shaded ? 1 << 7 : 0));
            _biff(0x43, v);
            break;
    }
#else
    unsigned char buf[16];
    size_t len;
    
    len = biff[_excel_format].define_cell_format(buf, sizeof buf,
                        font,
                        format,
                        (::align)align,
                        (::border)border,
                        shaded);
    std::ofstream::write((const char *)buf, len);
#endif
    _nxf++;
    if (_nxf < (ssize_t)(sizeof(_xf_def)/sizeof(_xf_def[0]))) {
        _xf_def[_nxf].format = format;
        _xf_def[_nxf].font = font;
        return cell_format_type(_nxf);
    }
    return -1;
}

ed::number_format_type
ed::oxlstream::define_number_format(const std::string &fmt)
{
#if 0
    _biff(0x1e, _to_buf(fmt.c_str()));
#else
    size_t len;
    unsigned char *buf = new unsigned char[len = 16 + fmt.length()];
    
    len = biff[_excel_format].define_number_format(buf, len, fmt.c_str());
    std::ofstream::write((const char *)buf, len);
    delete buf;
#endif
    return ++_nformats;
}

void ed::oxlstream::set_cell_format(cell_format_type format)
{
    if (format < 63 && ssize_t(format) <= _nformats)
        _xf_in_use = format;
}

#if 0
void ed::oxlstream::_define_style(cell_format_type format, uint8_t id)
{
    std::vector<uint8_t> v = _intel16(format + 0x8000);

    v.insert(v.end(), id);
    v.insert(v.end(), 255);
    _biff(0x293, v);
}
#endif


// Inherited unformatted I/O

ed::oxlstream &ed::oxlstream::write(const char *data, std::streamsize n)
{
    _write(data, n);
    return *this;
}

ed::oxlstream &ed::oxlstream::put(char c)
{
    _write(&c, 1);
    return *this;
}

// Dummy stream control

ed::oxlstream &ed::oxlstream::seekp(std::streampos)
{
    throw std::logic_error("seekp() undefined");
    return *this;
}

ed::oxlstream &ed::oxlstream::seekp(std::streamoff, std::ios_base::seekdir)
{
    throw std::logic_error("seekp() undefined");
    return *this;
}

std::streampos ed::oxlstream::tellp()
{
    throw std::logic_error("tellp() undefined");
    return 0;
}

// Special excel stream control

void ed::oxlstream::jump_to(size_t row, size_t col)
{
    if (row < _row)
        throw std::logic_error("Decreasing row");
    if (row == _row && col < _col)
        throw std::logic_error("Decreasing column on same row");
    if (row > 65536)
        throw std::out_of_range("Row out of range");
    if (col > 256)
        throw std::out_of_range("Column out of range");
    _row = row;
    _col = col;
}

// Inherited formatted I/O

ed::oxlstream &ed::oxlstream::operator<<(bool data)
{
    // FIXME
    // This can't work in other country excel version
    _write(data ? "=VRAI()" : "=FAUX()", 7);
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(short data)
{
    if (data >= 0)
        _write(uint16_t(data));
    else 
        _write(double(data));
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(unsigned short data)
{
    _write(uint16_t(data));
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(int data)
{
    if (data >= 0 && data < 65536)
        _write(uint16_t(data));
    else 
        _write(double(data));
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(unsigned int data)
{
    if (data < 65536)
        _write(uint16_t(data));
    else 
        _write(double(data));
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(long data)
{
    if (data >= 0 && data < 65536)
        _write(uint16_t(data));
    else 
        _write(double(data));
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(unsigned long data)
{
    if (data < 65536)
        _write(uint16_t(data));
    else 
        _write(double(data));
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(double data)
{
    _write(data);
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(long double data)
{
    throw std::out_of_range("cannot output long double");
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(const void *data)
{
    throw std::logic_error("operator<<(const void *) not implemented");
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(std::streambuf *data)
{
    throw std::logic_error("operator<<(std::streambuf *) not implemented");
    return *this;
}

#include <iostream>

std::ostream &ed::oxlstream::operator<<(std::ostream &(*fun)(std::ostream &))
{
//FIXME check std::endl
#if 0
    if (&fun == &(std::ostream &(*fun)(std::ostream &))) {
        jump_to(_row+1, 1);
        return *this;
    }
    return (*fun)(*this);
#else
    jump_to(_row+1, 1);
    return *this;
#endif
}

ed::oxlstream &ed::oxlstream::operator<<(std::ios &(*fun)(std::ios &))
{
    // Do nothing
    return *this;
}

ed::oxlstream &ed::oxlstream::operator<<(std::ios_base &(*fun)(std::ios_base &))
{
    // Do Nothing
    return *this;
}

// Inherited formatted I/O 

ed::oxlstream &operator<<(ed::oxlstream &str, char c)
{
    str.put(c);
    return str;
}

ed::oxlstream &operator<<(ed::oxlstream &str, signed char c)
{
    str.put(c);
    return str;
}

ed::oxlstream &operator<<(ed::oxlstream &str, unsigned char c)
{
    str.put(c);
    return str;
}

ed::oxlstream &operator<<(ed::oxlstream &str, const char *s)
{
    if (s)
        str.write(s, strlen(s));
    return str;
}

ed::oxlstream &operator<<(ed::oxlstream &str, const signed char *s)
{
    if (s)
        str.write((const char *)s, strlen((const char *)s));
    return str;
}

ed::oxlstream &operator<<(ed::oxlstream &str, const unsigned char *s)
{
    if (s)
        str.write((const char *)s, strlen((const char *)s));
    return str;
}

ed::oxlstream &operator<<(ed::oxlstream &str, const std::string &s)
{
    str.write(s.c_str(), s.size());
    return str;
}


// Utilities


void ed::oxlstream::_store_max_row_col(size_t row, size_t col)
{
    if (row > _max_row)
        _max_row = row;
    if (col > _max_col)
        _max_col = col;
}


std::vector<uint8_t> ed::oxlstream::_biff2_cell_attr() const
{
    std::vector<uint8_t> result;

    result.resize(3);
    result[0] = uint8_t(_xf_in_use);
    result[1] = uint8_t(
        (_xf_def[_xf_in_use].format | 0xc0) & _xf_def[_xf_in_use].font);
    result[2] = 0;
    return result;
}

void ed::oxlstream::_write(uint16_t val)
{
    std::vector<uint8_t> v;

    jump_to(_row, _col);
    _store_max_row_col(_row-1, _col-1);
    // 5.60 INTEGER
    switch (_excel_format) {
        case BIFF2:
        default:
            cat(v, _intel16(_row-1));
            cat(v, _intel16(_col-1));
            cat(v, _biff2_cell_attr());
            cat(v, _intel16(val));
            _biff(0x02, v);
            break;
    }
    jump_to(_row, _col+1);
}

void ed::oxlstream::_write(double val)
{
    std::vector<uint8_t> v;

    jump_to(_row, _col);
    _store_max_row_col(_row-1, _col-1);
    // 5.71 NUMBER
    switch (_excel_format) {
        case BIFF2:
        default:
            cat(v, _intel16(_row-1));
            cat(v, _intel16(_col-1));
            cat(v, _biff2_cell_attr());
            cat(v, _intel_double(val));
            _biff(0x03, v);
            break;
    }
    jump_to(_row, _col+1);
}

void ed::oxlstream::_write(const std::string &val)
{
    _write(val.c_str(), val.size());
}

void ed::oxlstream::_write(const char *val, size_t len)
{
    std::vector<uint8_t> v;

    jump_to(_row, _col);
    _store_max_row_col(_row-1, _col-1);
    // 5.63 LABEL
    switch (_excel_format) {
        case BIFF2:
        default:
            cat(v, _intel16(_row-1));
            cat(v, _intel16(_col-1));
            cat(v, _biff2_cell_attr());
            cat(v, _to_buf(val, len));
            _biff(0x04, v);
            break;
    }
    jump_to(_row, _col+1);
}
