/*
 * Copyright (C) 2018, 2021 Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XLS_H
#define XLS_H

/*
 *  libexcel:
 *  A tool to write Excel (BIFF2) files.
 *  This is not intended to produce highly formatted file.
 */

#include <stddef.h>
#include <stdint.h>

/*
 * Only BIFF2 is used for now (and probably forever)
 */

typedef enum {
    BIFF2,       /* Excel 2.0: most compatible */
} XLS_format;

typedef enum {
    FONT_REGULAR     = 0,
    FONT_BOLD        = 0x01,
    FONT_ITALIC      = 0x02,
    FONT_UNDERLINED  = 0x04,
    FONT_STRIKED     = 0x08,
    FONT_OUTLINED    = 0x10,
    FONT_SHADOWED    = 0x20,
} font_style;

typedef ssize_t font_type;

/*
 * Warning: color refuse to work!
 */

typedef enum {
    COLOR_AUTO       = 0x7fff,
    COLOR_BLACK      = 0,
    COLOR_WHITE      = 0xffffff,
    COLOR_RED        = 0xff0000,
    COLOR_GREEN      = 0x00ff00,
    COLOR_BLUE       = 0x0000ff,
    COLOR_YELLOW     = 0xffff00,
    COLOR_MAGENTA    = 0xff00ff,
    COLOR_CYAN       = 0x00ffff
} color;

typedef ssize_t cell_format_type;

typedef ssize_t number_format_type;
#define NUMBER_FORMAT_GENERAL 0 
#define NUMBER_FORMAT_DECIMAL0 1 
#define NUMBER_FORMAT_DECIMAL2 2 
#define NUMBER_FORMAT_DECIMAL0_THOUSANDS_SEPARATOR 3 
#define NUMBER_FORMAT_DECIMAL2_THOUSANDS_SEPARATOR 4 
#define NUMBER_FORMAT_CURRENCY0 5 
#define NUMBER_FORMAT_CURRENCY_RED0 6 
#define NUMBER_FORMAT_CURRENCY2 7 
#define NUMBER_FORMAT_CURRENCY_RED2 8 
#define NUMBER_FORMAT_PERCENT0 9 
#define NUMBER_FORMAT_PERCENT2 10 
#define NUMBER_FORMAT_SCIENTIFIC 11 
#define NUMBER_FORMAT_PERCENT0_PLUS 12 
#define NUMBER_FORMAT_PERCENT2_PLUS 13

typedef enum {
    ALIGN_AUTO,
    ALIGN_LEFT,
    ALIGN_CENTER,
    ALIGN_RIGHT,
    ALIGN_FILLED,
} align;

typedef enum {
    BORDER_NONE    = 0,
    BORDER_LEFT    = 0x01,
    BORDER_RIGHT   = 0x02,
    BORDER_TOP     = 0x04,
    BORDER_BOTTOM  = 0x08,
    BORDER_BOX = (BORDER_LEFT|BORDER_RIGHT|BORDER_TOP|BORDER_BOTTOM)
} border;

struct xls;

/*
 * XLS File handling
 */

struct xls *xls_open(const char *, XLS_format);
extern void xls_close(struct xls *);

/*
 * Page setup
 */

extern void xls_page_setup(struct xls *); 
extern void xls_header(struct xls *, const char *);
extern void xls_footer(struct xls *, const char *);

/*
 * Render setup
 */

extern void xls_row_col_headers(struct xls *);
extern void xls_grid_lines(struct xls *);

/*
 * Fonts
 */

extern font_type xls_define_font(struct xls *,
                const char *, size_t, font_style, color);

/*
 * Formats
 */

extern number_format_type xls_define_number_format(struct xls*, const char *);
extern cell_format_type xls_define_cell_format(struct xls *,
                   font_type, number_format_type, align, border, int);

/*
 * Data
 */

extern void xls_write_int(struct xls *,
                          unsigned, unsigned,
                          cell_format_type,
                          int);
extern void xls_write_double(struct xls *,
                             unsigned, unsigned, 
                             cell_format_type,
                             double);
extern void xls_write_str(struct xls *,
                          unsigned, unsigned,
                          cell_format_type,
                          const char *);

#endif
