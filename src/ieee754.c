/*
 * Copyright (C) 2012, 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <math.h>
#include <string.h>
#include "ieee754.h"

static const uint8_t
    inf_pos[] = { 0x7f, 0xf0, 0, 0, 0, 0, 0, 0 },
    inf_neg[] = { 0xff, 0xf8, 0, 0, 0, 0, 0, 0 },
    zero_pos[] = { 0, 0, 0, 0, 0, 0, 0, 0 },
    zero_neg[] = { 0x80, 0, 0, 0, 0, 0, 0, 0 };

#define EXPONENT_BIAS   1023    /*(2**10-1)*/
#define EXPONENT_FIRST  (-51)
#define EXPONENT_LAST   2047    /*(2**11-1)*/
#define FRACTION_BITS   52
#define MANTISSA_BITS   53

/*
 * Extract the exponent part of an IEEE754 double
 */

static int exponent(const uint8_t val[8])
{
    return (int)(val[0] & 0x7f) << 4 | (val[1] >> 4);
}

/*
 * Extract the mantissa part of an IEEE754 double
 */

static uint64_t mantissa(const uint8_t val[8])
{
    uint64_t pow2 = 2;
    int i;

    for (i = 1; i < FRACTION_BITS; i++)
        pow2 *= 2;

    return (uint64_t)(val[7]) |
            ((uint64_t)(val[6]) << 8) |
            ((uint64_t)(val[5]) << 8*2) |
            ((uint64_t)(val[4]) << 8*3) |
            ((uint64_t)(val[3]) << 8*4) |
            ((uint64_t)(val[2]) << 8*5) |
            (((uint64_t)(val[1]) & 0x0f) << 8*6) |
            pow2;
}

/*
 * Normalize in the range 2**53 > . > 2**52
 */

static void normalize(double val, uint64_t *mant, int *exp)
{
    double accum = val;
    int shift;

    *exp = 0;
    if (accum < pow(2.0, FRACTION_BITS)) {
        shift = 24;
        while (shift > 0) {
            if (accum < pow(2.0, MANTISSA_BITS - shift)) {
                accum *= pow(2.0, shift);
                *exp -= shift;
            }
            else {
                shift /= 2;
            }
        }
    }
    else if (accum >= pow(2.0, MANTISSA_BITS)) {
        shift = 8;
        while (shift > 0) {
            if (accum >= pow(2.0, FRACTION_BITS + shift)) {
                accum /= pow(2.0, shift);
                *exp += shift;
            }
            else {
                shift /= 2;
            }
        }
    }
    *mant = (uint64_t)accum;
}

double ieee754_to_double(const uint8_t val[8])
{
    int power;
    uint64_t fraction;
    double result;

    errno = 0;
    if ((val[0] & 0x7f) == 0 && 
            val[1] == 0 && val[2] == 0 && val[3] == 0 && val[4] == 0 &&
            val[5] == 0 && val[6] == 0 && val[7] == 0)
        return 0.0;

    power = exponent(val);
    fraction = mantissa(val);
    if (power == EXPONENT_LAST) {
        if (fraction /= 0x800) {
            errno = EDOM; /* not a number */
            return 0.0;
        }
        else {
            if (val[0] > 127)
                errno = ERANGE; /* negative overflow */
            else
                errno = ERANGE; /* positive overflow */
            return 0.0;
        }

    }
    else if (power == 0) { /* denormalized */
        fraction &= 0x0fffffffffffffff;
        power = EXPONENT_FIRST - EXPONENT_BIAS;
        result = (double)fraction * pow(2.0, power);
    }
    else { /* normalized */
        power -= EXPONENT_BIAS + FRACTION_BITS;
        result = (double)fraction * pow(2.0, power);
    }
    if (val[0] > 127)
        return -result;
    else
        return result;
}

int is_nan(const uint8_t val[8])
{
    return exponent(val) == EXPONENT_LAST &&
            mantissa(val) == pow(2.0, FRACTION_BITS);
}

int is_zero(const uint8_t val[8])
{
    return (val[0] == 0 || val[0] == 0x80) &&
                    val[1] == 0 && val[2] == 0 && val[3] == 0 &&
                    val[4] == 0 && val[5] == 0 && val[6] == 0 &&
                    val[7] == 0;
}

int is_negative(const uint8_t val[8])
{
    return val[0] > 127;
}

int is_real(const uint8_t val[8])
{
    return exponent(val) < EXPONENT_LAST;
}

uint8_t *double_to_ieee754(double val, uint8_t buf[8])
{
    uint64_t mant;
    int exp;
    uint8_t sign = 0;

    if (val == 0.0) {
        memset(buf, 0, 8);
        return buf;
    }
    if (val > 0.0)
        normalize(val, &mant, &exp);
    else {
        normalize(-val, &mant, &exp);
        sign = 128; /* 2**7 */
    }
    exp += EXPONENT_BIAS + FRACTION_BITS;
    if (exp < EXPONENT_FIRST) {
        memset(buf, 0, 8); /* underflow, result is zero */
        return buf;
    }
    else if (exp >= EXPONENT_LAST) {
        if (sign == 0)
            memcpy(buf, inf_pos, 8); /* overflow, positive infinity */
        else
            memcpy(buf, inf_neg, 8); /* overflow, negative infinity */
        return buf;
    }
    else if (exp <= 0) { /* denormalized */
        mant >>= 1 - exp;
        exp = 0;
    }
#if 1
    buf[0] = sign | (uint8_t)(exp / 16);
    buf[1] = (uint8_t)((mant >> 8*6) & 0x0f) | (uint8_t)((exp % 16) << 4);
    buf[2] = (uint8_t)((mant >> 8*5) & 0xff);
    buf[3] = (uint8_t)((mant >> 8*4) & 0xff);
    buf[4] = (uint8_t)((mant >> 8*3) & 0xff);
    buf[5] = (uint8_t)((mant >> 8*2) & 0xff);
    buf[6] = (uint8_t)((mant >> 8) & 0xff);
    buf[7] = (uint8_t)(mant & 0xff);
#else
    buf[7] = sign | (uint8_t)(exp / 16);
    buf[6] = (uint8_t)((mant >> 8*6) & 0x0f) | (uint8_t)((exp % 16) << 4);
    buf[5] = (uint8_t)((mant >> 8*5) & 0xff);
    buf[4] = (uint8_t)((mant >> 8*4) & 0xff);
    buf[3] = (uint8_t)((mant >> 8*3) & 0xff);
    buf[2] = (uint8_t)((mant >> 8*2) & 0xff);
    buf[1] = (uint8_t)((mant >> 8) & 0xff);
    buf[0] = (uint8_t)(mant & 0xff);
#endif
    return buf;
}
