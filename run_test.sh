#!/bin/sh

rm -f ./data/alignment.xls
./test/alignment ./data/alignment.xls &&
hexdump -C ./data/alignment.xls

rm -f ./data/border.xls
./test/border ./data/border.xls &&
hexdump -C ./data/border.xls

rm -f ./data/color.xls
./test/color ./data/color.xls &&
hexdump -C ./data/color.xls

rm -f ./data/polices.xls
./test/polices ./data/polices.xls &&
hexdump -C ./data/polices.xls

rm -f ./data/font_style.xls
./test/font_style ./data/font_style.xls &&
hexdump -C ./data/font_style.xls

rm -f ./data/format.xls
./test/format ./data/format.xls &&
hexdump -C ./data/format.xls

rm -f ./data/x0.xls
./test/test_excel ./data/x0.xls &&
hexdump -C ./data/x0.xls
