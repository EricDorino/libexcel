/*
 * Copyright (C) 2018, 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIFF2_H
#define BIFF2_H

#include "biff.h"

extern size_t biff2_bof(uint8_t *, size_t); 
extern size_t biff2_eof(uint8_t *, size_t, size_t, size_t); 
extern size_t biff2_page_setup(uint8_t *, size_t); 
extern size_t biff2_header(uint8_t *, size_t, const char *);
extern size_t biff2_footer(uint8_t *, size_t, const char *);
extern size_t biff2_row_col_headers(uint8_t *, size_t);
extern size_t biff2_grid_lines(uint8_t *, size_t);
extern size_t biff2_define_font(uint8_t *, size_t,
                                const char *, size_t, font_style, color);
extern size_t biff2_define_number_format(uint8_t *, size_t, const char *);
extern size_t biff2_define_cell_format(uint8_t *, size_t,
                                  font_type, number_format_type,
                                  align, border, int);
extern size_t biff2_n_builtin_formats(void);
extern size_t biff2_data_uint16(uint8_t *, size_t,
                                unsigned, unsigned,
                                number_format_type,
                                cell_format_type, 
                                font_type,
                                uint16_t);
extern size_t biff2_data_double(uint8_t *, size_t,
                                unsigned, unsigned,
                                number_format_type,
                                cell_format_type,
                                font_type,
                                double);
extern size_t biff2_data_str(uint8_t *, size_t,
                             unsigned, unsigned,
                             number_format_type,
                             cell_format_type,
                             font_type,
                             const char *);
#endif
