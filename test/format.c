/*
 * Copyright (C) 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xls.h>

struct {
    char *what;
    number_format_type number_format;
    cell_format_type cell_format;
} data[] = {
    {"General", NUMBER_FORMAT_GENERAL, -1},
    {"Decimal0", NUMBER_FORMAT_DECIMAL0, -1},
    {"Decimal2", NUMBER_FORMAT_DECIMAL2, -1},
    {"Decimal0 Thousands Separator",
        NUMBER_FORMAT_DECIMAL0_THOUSANDS_SEPARATOR, -1},
    {"Decimal2 Thousands Separator",
        NUMBER_FORMAT_DECIMAL2_THOUSANDS_SEPARATOR, -1},
    {"Currency0", NUMBER_FORMAT_CURRENCY0, -1},
    {"Currency Red0", NUMBER_FORMAT_CURRENCY_RED0, -1},
    {"Currency2", NUMBER_FORMAT_CURRENCY2, -1},
    {"Currency Red2", NUMBER_FORMAT_CURRENCY_RED2, -1},
    {"Percent0", NUMBER_FORMAT_PERCENT0, -1},
    {"Percent2", NUMBER_FORMAT_PERCENT2, -1},
    {"Scientific", NUMBER_FORMAT_SCIENTIFIC, -1},
    {"Percent0 Plus", NUMBER_FORMAT_PERCENT0_PLUS, -1},
    {"Percent2 Plus", NUMBER_FORMAT_PERCENT2_PLUS, -1},
    {NULL, 0, -1}
};

int main(int argc, char **argv)
{
    struct xls *xls;

    if (argc !=  2) {
        fprintf(stderr, "usage: %s <output file>\n", basename(argv[0]));
        return EXIT_FAILURE;
    }

    if (!(xls = xls_open(argv[1], BIFF2))) {
        fprintf(stderr, "cannot open file %s: %s\n", argv[1], strerror(errno));
        return EXIT_FAILURE;
    }

    xls_header(xls, argv[1]);
    xls_footer(xls, "Page &P");
    xls_grid_lines(xls);
    xls_row_col_headers(xls);
    xls_page_setup(xls);

    font_type font = xls_define_font(xls, 
            "DejaVu Sans",
            10, 
            FONT_REGULAR,
            COLOR_AUTO);

    for (unsigned i = 0; data[i].what; i++) {
        data[i].cell_format = xls_define_cell_format(xls, 
            font, 
            data[i].number_format,
            ALIGN_AUTO,
            BORDER_NONE,
            0);
    }

    unsigned row = 0, col = 0;

    for (unsigned i = 0; data[i].what; i++) {
        fprintf(stderr, "%d: format=%ld\n", i, data[i].cell_format);
        xls_write_str(xls, row, col, data[i].cell_format, data[i].what);
        xls_write_int(xls, row, col+1, data[i].cell_format, 9876);
        xls_write_double(xls, row, col+2, data[i].cell_format, 9876.54);
        row++;
    }

    return EXIT_SUCCESS;
}
