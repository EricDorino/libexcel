/*
 * Copyright (C) 2018, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "ieee754.h"

static void print(double x)
{
    uint8_t buf[8];

    (void)double_to_ieee754(x, buf);
    printf("%e = %e\n", x, ieee754_to_double(buf));
}

int main(int argc, char **argv)
{
    if (argc !=  1) {
        fprintf(stderr, "usage: ieee754\n");
        return EXIT_FAILURE;
    }
    print(1.0);
    print(0.5e2);
    print(1.5e22);
    print(1.5e-22);
    print(-1.5e12);
    print(5.0);
    print(5.5);
    return EXIT_SUCCESS;
}
