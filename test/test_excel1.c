/*
 * Copyright (C) 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xls.h>

int main(int argc, char **argv)
{
    struct xls *xls;

    if (argc !=  2) {
        fprintf(stderr, "usage: test_excel <output file>\n");
        return EXIT_FAILURE;
    }

    if (!(xls = xls_open(argv[1], BIFF2))) {
        fprintf(stderr, "cannot open file %s: %s\n", argv[1], strerror(errno));
        return EXIT_FAILURE;
    }

    xls_header(xls, argv[1]);
    xls_footer(xls, "Page &P");
    xls_grid_lines(xls);
    xls_row_col_headers(xls);
    xls_page_setup(xls, 100, 1, 0, ORIENTATION_PORTRAIT, 0);

#define SIZE 63
    
    font_type font[SIZE];
    cell_format_type format[SIZE];

    for (unsigned i = 0; i < SIZE; i++)
        font[i] = format[i] = -1;

    font[0] = xls_define_font(xls, 
            "DejaVu Sans", 10, 
            FONT_REGULAR, COLOR_AUTO);
    format[0] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_NONE,
            0);

    font[1] = xls_define_font(xls, 
            "DejaVu Serif", 10, 
            FONT_REGULAR, COLOR_AUTO);
    format[1] = xls_define_cell_format(xls, 
            font[1], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_NONE,
            0);

    font[2] = xls_define_font(xls, 
            "Nimbus Sans", 10, 
            FONT_REGULAR, COLOR_AUTO);
    format[2] = xls_define_cell_format(xls, 
            font[2], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_NONE,
            0);

    font[3] = xls_define_font(xls, 
            "Nimbus Sans Narrow", 10, 
            FONT_REGULAR, COLOR_AUTO);
    format[3] = xls_define_cell_format(xls, 
            font[3], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_NONE,
            0);

    font[4] = xls_define_font(xls, 
            "Latin Modern Mono", 10, 
            FONT_REGULAR, COLOR_AUTO);
    format[4] = xls_define_cell_format(xls, 
            font[4], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_NONE,
            0);

    format[5] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_LEFT,
            BORDER_NONE,
            0);

    format[6] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_CENTER,
            BORDER_NONE,
            0);

    format[7] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_RIGHT,
            BORDER_NONE,
            0);

    format[8] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_FILLED,
            BORDER_NONE,
            0);

    format[9] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_LEFT,
            0);

    format[10] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_RIGHT,
            0);

    format[11] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_TOP,
            0);

    format[12] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOTTOM,
            0);

    format[13] = xls_define_cell_format(xls, 
            font[0], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOX,
            0);

    font[5] = xls_define_font(xls, 
            "Latin Modern Mono", 12, 
            FONT_REGULAR, COLOR_BLACK);
    format[14] = xls_define_cell_format(xls, 
            font[5], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOX,
            0);

    font[6] = xls_define_font(xls, 
            "Latin Modern Mono", 12, 
            FONT_REGULAR, COLOR_RED);
    format[15] = xls_define_cell_format(xls, 
            font[6], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOX,
            0);

    font[7] = xls_define_font(xls, 
            "Latin Modern Mono", 12, 
            FONT_REGULAR, COLOR_GREEN);
    format[16] = xls_define_cell_format(xls, 
            font[7], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOX,
            0);

    font[8] = xls_define_font(xls, 
            "Latin Modern Mono", 12, 
            FONT_REGULAR, COLOR_BLUE);
    format[17] = xls_define_cell_format(xls, 
            font[8], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOX,
            0);

    font[9] = xls_define_font(xls, 
            "Latin Modern Mono", 12, 
            FONT_REGULAR, COLOR_YELLOW);
    format[18] = xls_define_cell_format(xls, 
            font[9], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOX,
            0);

    font[10] = xls_define_font(xls, 
            "Latin Modern Mono", 12, 
            FONT_REGULAR, COLOR_MAGENTA);
    format[19] = xls_define_cell_format(xls, 
            font[10], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOX,
            0);

    font[11] = xls_define_font(xls, 
            "Latin Modern Mono", 12, 
            FONT_REGULAR, COLOR_CYAN);
    format[20] = xls_define_cell_format(xls, 
            font[11], 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_BOX,
            0);

    unsigned row = 0, col = 0;

    for (unsigned i = 0; i < SIZE && i < 5; i++)
        if (format[i] >= 0) {
            fprintf(stderr, "%d: format=%ld\n",
                    i, format[i]);
            xls_write_str(xls, row++, col, format[i], "Bonjour");
            xls_write_int(xls, row++, col, format[i], 42);
            xls_write_double(xls, row++, col, format[i], 42.5);
        }

    row = 0; col = 1;

    for (unsigned i = 5; i < SIZE && i < 9; i++)
        if (format[i] >= 0) {
            fprintf(stderr, "%d: format=%ld\n",
                    i, format[i]);
            xls_write_str(xls, row++, col, format[i], "Bonjour");
            xls_write_int(xls, row++, col, format[i], 42);
            xls_write_double(xls, row++, col, format[i], 42.5);
        }

    row = 0; col = 3;

    for (unsigned i = 9; i < SIZE && i < 14; i++)
        if (format[i] >= 0) {
            fprintf(stderr, "%d: format=%ld\n",
                    i, format[i]);
            xls_write_str(xls, row, col, format[i], "Bonjour");
            row += 2;
            xls_write_int(xls, row, col, format[i], 42);
            row += 2;
            xls_write_double(xls, row, col, format[i], 42.5);
            row += 2;
        }

    row = 0; col = 5;
    for (unsigned i = 14; i < SIZE && i < 21; i++)
        if (format[i] >= 0) {
            fprintf(stderr, "%d: format=%ld\n",
                    i, format[i]);
            xls_write_str(xls, row++, col, format[i], "Bonjour");
            xls_write_int(xls, row++, col, format[i], 42);
            xls_write_double(xls, row++, col, format[i], 42.5);
        }

    xls_close(xls);
    return EXIT_SUCCESS;
}
