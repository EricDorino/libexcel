/*
 * Copyright (C) 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cstring>
#include <stdexcept>
#include <oxlstream>
#include "ieee754.h"

std::vector<uint8_t>
ed::oxlstream::_intel16(uint16_t val)
{
    std::vector<uint8_t> v;

    v.resize(2);
    v[0] = val & 255;
    v[1] = (val / 256) & 255;
    return v;
}

std::vector<uint8_t>
ed::oxlstream::_intel_double(double val)
{
    uint8_t v[8];
    
    (void)double_to_ieee754(val, v);

    std::vector<uint8_t> result;
    result.resize(8);
    for (size_t i = 0; i < 8; i++)
        result[i] = v[7-i];
    return result;
}

std::vector<uint8_t>
ed::oxlstream::_to_buf(const std::string &str)
{
    return _to_buf(str.c_str(), str.size());
}

std::vector<uint8_t>
ed::oxlstream::_to_buf(const char *str)
{
    if (!str)
        throw std::invalid_argument("_to_buf(): null pointer");
    return _to_buf(str, strlen(str));
}

std::vector<uint8_t>
ed::oxlstream::_to_buf(const char *str, size_t len)
{
    std::vector<uint8_t> result;

    if (!str)
        throw std::invalid_argument("_to_buf(): null pointer");
    if (len > 255)
        throw std::length_error("_to_buff(): cannot handle len > 255");
    result.resize(len+1);
    result[0] = len;
    for (size_t i = 0; i < len; i++)
        result[i+1] = str[i];
    return result;
}

std::ostream &
ed::oxlstream::_biff(uint16_t id, const std::vector<uint8_t> v)
{
    std::vector<uint8_t> idv = _intel16(id), vs = _intel16(v.size());

#if 0
    if (v.size() > 255)
        throw std::range_error("buffer overrun");
#endif
    std::ofstream::put(idv[0]);
    std::ofstream::put(idv[1]);
    std::ofstream::put(vs[0]); 
    std::ofstream::put(vs[1]);
    for (size_t  i = 0; i < v.size(); i++)
        std::ofstream::put(v[i]);
    return *this;
}

std::ostream &
ed::oxlstream::_biff(uint16_t id, const char *v)
{
    std::vector<uint8_t> idv = _intel16(id),
        vs = _intel16(v ? strlen(v)+1 : 0);

#if 0   
    if (v && strlen(v) > 255)
        throw std::range_error("buffer overrun");
#endif
    std::ofstream::put(idv[0]); 
    std::ofstream::put(idv[1]);
    std::ofstream::put(vs[0]); 
    std::ofstream::put(vs[1]);
    if (v) {
        std::ofstream::put(strlen(v));
        for (size_t  i = 0; i < strlen(v); i++)
            std::ofstream::put(v[i]);
    }
    return *this;
}

std::vector<uint8_t> &
ed::cat(std::vector<uint8_t> &v1, const std::vector<uint8_t> &v2)
{
    v1.insert(v1.end(), v2.begin(), v2.end());
    return v1;
}

ed::font_style
operator|(ed::font_style left, ed::font_style right)
{
    return ed::font_style(left|right);
}
