/*
 * Copyright (C) 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <string>
#include <oxlstream>

static void style(ed::oxlstream &xls,
                  const char *font, size_t size,
                  ed::font_style style, 
                  const char *data)
{
    std::ostringstream sstr;
    sstr << size;
    std::string d = std::string(font) + " " +
                    sstr.str() +  " " +
                    std::string(data);

    xls.set_cell_format(
        xls.define_cell_format(
            xls.define_font(font, size, style),
            ed::General));
    xls << d;
}

int main(int argc, char **argv)
{
    if (argc !=  2) {
        std::cerr << "usage: test_excel <output file>" << std::endl;
        return 1;
    }

    ed::oxlstream xls(argv[1]);

    if (!xls.good()) {
        std::cerr << "test_excel: cannot open output file" << std::endl;
        return 1;
    }

    xls.header("Test "+std::string(argv[1]));
    xls.footer("Page &P");
    xls.grid_lines();
    xls.row_col_headers();
    xls.page_setup();

    xls.write("Hello", 5);
    xls.write("Le", 2);
    xls.write("Monde", 5);

    xls.jump_to(3, 1);

    xls << "Bonjour" << "Le" << "Monde" << "123" << "128000";
    xls << 9999 << 999999 << 9.9e2;
    xls << std::string("Salut") << std::string("les amis");

    xls.jump_to(4, 2);
    xls << "Ligne 1" << std::endl;  
    xls << "Ligne 2" << std::endl;  
    xls << "Ligne 3" << std::endl;  

    xls.set_cell_format(xls.default_cell_format());
    xls << " ";
    xls.set_cell_format(xls.define_cell_format(xls.default_font(),
                   ed::General, ed::General_Align, ed::Right));
    xls << "General Right" << std::endl;
    xls << std::endl;
    xls.set_cell_format(xls.default_cell_format());
    xls << " ";
    xls.set_cell_format(xls.define_cell_format(xls.default_font(),
                   ed::General, ed::General_Align, ed::Left));
    xls << "General Left" << std::endl;
    xls << std::endl;
    xls.set_cell_format(xls.default_cell_format());
    xls << " ";
    xls.set_cell_format(xls.define_cell_format(xls.default_font(),
                   ed::General, ed::General_Align, ed::Top));
    xls << "General Top" << std::endl;
    xls << std::endl;
    xls.set_cell_format(xls.default_cell_format());
    xls << " ";
    xls.set_cell_format(xls.define_cell_format(xls.default_font(),
                   ed::General, ed::General_Align, ed::Bottom));
    xls << "General Bottom" << std::endl;
    xls << std::endl;
    xls.set_cell_format(xls.default_cell_format());
    xls << " ";
    xls.set_cell_format(xls.define_cell_format(xls.default_font(),
                   ed::General, ed::General_Align, ed::Box));
    xls << "General Box" << std::endl;
    xls << std::endl;
    xls.set_cell_format(xls.default_cell_format());

#if 1
    style(xls, "DejaVu Sans", 8, ed::Bold, "Bold");
    style(xls, "DejaVu Sans", 8, ed::Italic, "Italic");
    style(xls, "DejaVu Sans", 8, ed::Underlined, "Underlined");
    style(xls, "DejaVu Sans", 8, ed::Striked, "Striked");
    style(xls, "DejaVu Sans", 8, ed::Outlined, "Outlined");
    style(xls, "DejaVu Sans", 8, ed::Shadowed, "Shadowed");
    //style(xls, "DejaVu Sans", 8, ed::Italic | ed::Bold, "Bold Italic");
    xls << std::endl;
#endif

#if 1
    style(xls, "Latin Modern Mono", 10, ed::Bold, "Bold");
    style(xls, "Latin Modern Mono", 10, ed::Italic, "Italic");
    style(xls, "Latin Modern Mono", 10, ed::Underlined, "Underlined");
    style(xls, "Latin Modern Mono", 10, ed::Striked, "Striked");
    style(xls, "Latin Modern Mono", 10, ed::Outlined, "Outlined");
    style(xls, "Latin Modern Mono", 10, ed::Shadowed, "Shadowed");
    //style(xls, "Latin Modern Mono", 10, ed::Italic | ed::Bold, "Bold Italic");
    xls << std::endl;
#endif

    xls.close();
    return 0;
}
