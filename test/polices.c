/*
 * Copyright (C) 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xls.h>

struct {
    char *name;
    size_t size;
    font_type font;
    cell_format_type format;
} data[] = {
    {"DejaVu Sans", 10, -1, -1},
    {"DejaVu Serif", 10, -1, -1},
    {"Nimbus Sans", 10, -1, -1},
    {"Nimbus Sans Narrow", 10, -1, -1},
    {"Latin Modern Mono", 10, -1, -1},
    {"DejaVu Sans", 16, -1, -1},
    {"DejaVu Serif", 18, -1, -1},
    {"Nimbus Sans", 20, -1, -1},
    {"Nimbus Sans Narrow", 20, -1, -1},
    {"Latin Modern Mono", 22, -1, -1},
    {NULL, 0, -1, -1}
};

int main(int argc, char **argv)
{
    struct xls *xls;

    if (argc !=  2) {
        fprintf(stderr, "usage: %s <output file>\n", basename(argv[0]));
        return EXIT_FAILURE;
    }

    if (!(xls = xls_open(argv[1], BIFF2))) {
        fprintf(stderr, "cannot open file %s: %s\n", argv[1], strerror(errno));
        return EXIT_FAILURE;
    }

    xls_header(xls, argv[1]);
    xls_footer(xls, "Page &P");
    xls_grid_lines(xls);
    xls_row_col_headers(xls);
    xls_page_setup(xls);

    for (unsigned i = 0; data[i].name; i++) {
        data[i].font = xls_define_font(xls, 
            data[i].name,
            data[i].size, 
            FONT_REGULAR,
            COLOR_AUTO);
        data[i].format = xls_define_cell_format(xls, 
            data[i].font, 
            NUMBER_FORMAT_GENERAL,
            ALIGN_AUTO,
            BORDER_NONE,
            0);
    }

    unsigned row = 0, col = 0;

    for (unsigned i = 0; data[i].name; i++) {
        fprintf(stderr, "%d: format=%ld\n", i, data[i].format);
        xls_write_str(xls, row, col, data[i].format, data[i].name);
        xls_write_int(xls, row, col+1, data[i].format, data[i].size);
        xls_write_double(xls, row, col+2, data[i].format,
                    (double)data[i].size);
        ++row;
    }

    return EXIT_SUCCESS;
}
