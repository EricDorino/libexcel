/*
 * Copyright (C) 2012, 2021 Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IEEE754_H
#define IEEE754_H
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

extern uint8_t *double_to_ieee754(double, uint8_t[8]);
extern double ieee754_to_double(const uint8_t[8]);
extern int is_nan(const uint8_t[8]),
	       is_zero(const uint8_t[8]), 
	       is_negative(const uint8_t[8]), 
	       is_real(const uint8_t[8]);

#ifdef __cplusplus
}
#endif

#endif
